package com.kmarinos.whereismydesk.domain.dev;

import com.kmarinos.whereismydesk.domain.model.CollisionResolution;
import com.kmarinos.whereismydesk.domain.model.Desk;
import com.kmarinos.whereismydesk.domain.model.DeskAvailability;
import com.kmarinos.whereismydesk.domain.model.Employee;
import com.kmarinos.whereismydesk.domain.model.Reservation;
import com.kmarinos.whereismydesk.domain.model.ReserveDeskPermission;
import com.kmarinos.whereismydesk.domain.model.Room;
import com.kmarinos.whereismydesk.domain.model.RoomOccupancy;
import com.kmarinos.whereismydesk.domain.model.RoomOwnership;
import com.kmarinos.whereismydesk.domain.model.Team;
import com.kmarinos.whereismydesk.domain.model.Workday;
import com.kmarinos.whereismydesk.domain.model.WorkingHistory;
import com.kmarinos.whereismydesk.domain.model.Workplace;
import com.kmarinos.whereismydesk.domain.model.TeamWorkplaceOwnership;
import com.kmarinos.whereismydesk.domain.repository.CollisionResolutionRepository;
import com.kmarinos.whereismydesk.domain.repository.DeskAvailabilityRepository;
import com.kmarinos.whereismydesk.domain.repository.DeskRepository;
import com.kmarinos.whereismydesk.domain.repository.EmployeeRepository;
import com.kmarinos.whereismydesk.domain.repository.ReservationRepository;
import com.kmarinos.whereismydesk.domain.repository.ReserveDeskPermissionRepository;
import com.kmarinos.whereismydesk.domain.repository.RoomOccupancyRepository;
import com.kmarinos.whereismydesk.domain.repository.RoomOwnershipRepository;
import com.kmarinos.whereismydesk.domain.repository.RoomRepository;
import com.kmarinos.whereismydesk.domain.repository.TeamRepository;
import com.kmarinos.whereismydesk.domain.repository.WorkDayRepository;
import com.kmarinos.whereismydesk.domain.repository.WorkingHistoryRepository;
import com.kmarinos.whereismydesk.domain.repository.TeamWorkplaceOwnershipRepository;
import com.kmarinos.whereismydesk.domain.repository.WorkplaceRepository;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
@RequiredArgsConstructor
public class DevDomain {

  private final TeamRepository teamRepository;
  private final WorkDayRepository workDayRepository;
  private final WorkplaceRepository workplaceRepository;
  private final RoomRepository roomRepository;
  private final DeskRepository deskRepository;
  private final EmployeeRepository employeeRepository;
  private final ReserveDeskPermissionRepository reserveDeskPermissionRepository;
  private final RoomOwnershipRepository roomOwnershipRepository;
  private final RoomOccupancyRepository roomOccupancyRepository;
  private final TeamWorkplaceOwnershipRepository teamWorkplaceOwnershipRepository;
  private final ReservationRepository reservationRepository;
  private final WorkingHistoryRepository workingHistoryRepository;
  private final CollisionResolutionRepository collisionResolutionRepository;
  private final DeskAvailabilityRepository deskAvailabilityRepository;

  Random random = new Random();

//  @EventListener(ApplicationReadyEvent.class)
  public void init() {
    Map<String, Workplace> workplaceMap = initWorkplaces();
    Map<String, Room> roomMap = initRooms();
    Map<String, Desk> deskMap = initDesks();

    Map<String, Workday> workdayMap = initWorkdays();

    Map<String, Team> teamsMap = initTeams();
    Map<String, Employee> employeeMap = initEmployees();

    //Desk dependencies
    deskMap.get("Desk 1.1").setRoom(roomMap.get("Room 1"));
    deskMap.get("Desk 1.2").setRoom(roomMap.get("Room 1"));
    deskMap.get("Desk 1.3").setRoom(roomMap.get("Room 1"));
    deskMap.get("Desk 1.4").setRoom(roomMap.get("Room 1"));
    deskMap.get("Desk 2.1").setRoom(roomMap.get("Room 2"));
    deskMap.get("Desk 3.1").setRoom(roomMap.get("Room 3"));
    deskMap.get("Desk 3.2").setRoom(roomMap.get("Room 3"));
    //Room Dependencies
    roomMap.get("Room 1").setWorkplace(workplaceMap.get("Office"));
    roomMap.get("Room 2").setWorkplace(workplaceMap.get("Office"));
    roomMap.get("Room 3").setWorkplace(workplaceMap.get("Office"));
    //Employee dependencies
    teamRepository.saveAll(teamsMap.values());
    employeeMap.get("Employee 1.1").setTeam(teamsMap.get("Team A"));
    employeeMap.get("Employee 1.2").setTeam(teamsMap.get("Team A"));
    employeeMap.get("Employee 1.3").setTeam(teamsMap.get("Team A"));
    employeeMap.get("Employee 1.4").setTeam(teamsMap.get("Team A"));
    employeeMap.get("Employee 1.5").setTeam(teamsMap.get("Team A"));
    employeeMap.get("Employee 1.6").setTeam(teamsMap.get("Team A"));
    employeeMap.get("Employee 2.1").setTeam(teamsMap.get("Team B"));
    employeeMap.get("Employee 2.2").setTeam(teamsMap.get("Team B"));
    //Team dependencies
    teamsMap.get("Team A").setTeamLead(employeeMap.get("Employee 1.1"));
    teamsMap.get("Team B").setTeamLead(employeeMap.get("Employee 2.1"));

    /***** PERSIST *****/
    workDayRepository.saveAll(workdayMap.values());
    workplaceRepository.saveAll(workplaceMap.values());
    roomRepository.saveAll(roomMap.values());
    deskRepository.saveAll(deskMap.values());
    employeeRepository.saveAll(employeeMap.values());
    teamRepository.saveAll(teamsMap.values());

    //Desk permissions
    List<ReserveDeskPermission> reserveDeskPermissionList = initReserveDeskPermissions(workdayMap,
        deskMap, employeeMap);
    reserveDeskPermissionRepository.saveAll(reserveDeskPermissionList);
    //Desk availability
    List<DeskAvailability>deskAvailabilityList = initDeskAvailabilities(deskMap,workdayMap);
    deskAvailabilityRepository.saveAll(deskAvailabilityList);
    //Room ownership
    List<RoomOwnership> roomOwnershipList = initRoomOwnerships(teamsMap, roomMap, workdayMap);
    roomOwnershipRepository.saveAll(roomOwnershipList);
    //Room occupancy
    List<RoomOccupancy> roomOccupancies = initRoomOccupancies(roomMap, workdayMap);
    roomOccupancyRepository.saveAll(roomOccupancies);
    //workplace ownership
    List<TeamWorkplaceOwnership> teamWorkplaceOwnershipList = initWorkplaceOwnerships(workplaceMap,
        teamsMap, workdayMap);
    teamWorkplaceOwnershipRepository.saveAll(teamWorkplaceOwnershipList);
    //reservation
    List<Reservation> reservationList = initDummyReservations(employeeMap, workdayMap, workplaceMap,
        deskMap);
    reservationRepository.saveAll(reservationList);
    //Working history
    List<WorkingHistory> workingHistoryList = initDummyWorkingHistory(employeeMap, workdayMap,
        workplaceMap);
    workingHistoryRepository.saveAll(workingHistoryList);
    //collision resolution
    String collisionId = UUID.randomUUID().toString();
    CollisionResolution col1_1 = CollisionResolution.builder()
        .reservation(reservationList.get(0))
        .collisionId(collisionId)
        .build();
    CollisionResolution col1_2 = CollisionResolution.builder()
        .reservation(reservationList.get(1))
        .collisionId(collisionId)
        .build();
    collisionResolutionRepository.save(col1_1);
    collisionResolutionRepository.save(col1_2);


  }

  private List<DeskAvailability> initDeskAvailabilities(Map<String, Desk> deskMap,
      Map<String, Workday> workdayMap) {
    var list = new ArrayList<DeskAvailability>();
    for (Desk desk : deskMap.values()) {
      for (Workday workday : workdayMap.values()) {
        list.add(DeskAvailability.builder()
                .desk(desk)
                .workday(workday)
                .available(true)
            .build());
      }
    }
    return list;
  }

  private List<RoomOccupancy> initRoomOccupancies(Map<String, Room> roomMap,
      Map<String, Workday> workdayMap) {
    var list = new ArrayList<RoomOccupancy>();
    for (Room room : roomMap.values()) {
      for (Workday workday : workdayMap.values()) {
        list.add(RoomOccupancy.builder()
            .desksAvailable(random.nextInt(4))
            .room(room)
            .workday(workday)
            .build());
      }
    }
    return list;
  }

  private List<WorkingHistory> initDummyWorkingHistory(Map<String, Employee> employeeMap,
      Map<String, Workday> workdayMap, Map<String, Workplace> workplaceMap) {
    var list = new ArrayList<WorkingHistory>();

    for (Employee employee : employeeMap.values()) {
      for (Workday workday : workdayMap.values()) {
        double chance = random.nextDouble();
        Workplace workplace = null;
        if (chance < 0.05) {
          continue;
        } else if (chance < 0.15) {
          workplace = workplaceMap.get("Field work");
        } else if (chance < 0.7) {
          workplace = workplaceMap.get("Office");
        } else {
          workplace = workplaceMap.get("From Home");
        }
        list.add(WorkingHistory.builder()
            .employee(employee)
            .workplace(workplace)
            .workday(workday)
            .build());
      }

    }
    return list;
  }

  private List<Reservation> initDummyReservations(Map<String, Employee> employeeMap,
      Map<String, Workday> workdayMap, Map<String, Workplace> workplaceMap,
      Map<String, Desk> deskMap) {
    var list = new ArrayList<Reservation>();
    for (Employee employee : employeeMap.values()) {
      for (Workday workday : workdayMap.values()) {
        double chance = random.nextDouble();
        Workplace workplace = null;
        Desk desk = null;
        if (chance < 0.05) {
          continue;
        } else if (chance < 0.15) {
          workplace = workplaceMap.get("Field work");
        } else if (chance < 0.7) {
          workplace = workplaceMap.get("Office");
          desk = getRandom(deskMap);
        } else {
          workplace = workplaceMap.get("From Home");
        }
        list.add(Reservation.builder()
            .desk(desk)
            .employee(employee)
            .workplace(workplace)
            .workday(workday)
            .build());
      }
    }

    return list;
  }

  private <T> T getRandom(Map<?, T> map) {
    Object[] values = map.values().toArray();
    return (T) values[random.nextInt(values.length)];
  }

  private List<TeamWorkplaceOwnership> initWorkplaceOwnerships(Map<String, Workplace> workplaceMap,
      Map<String, Team> teamsMap, Map<String, Workday> workdayMap) {
    var resultList = new ArrayList<TeamWorkplaceOwnership>();

    for (Workplace workplace : workplaceMap.values()) {
      for (Team team : teamsMap.values()) {
        for (Workday workday : workdayMap.values()) {
          resultList.add(TeamWorkplaceOwnership.builder()
              .team(team)
              .workplace(workplace)
              .workday(workday)
              .build());
        }
      }
    }

    return resultList;
  }

  private List<RoomOwnership> initRoomOwnerships(Map<String, Team> teamsMap,
      Map<String, Room> roomMap, Map<String, Workday> workdayMap) {
    var resultList = new ArrayList<RoomOwnership>();
    for (Team team : teamsMap.values()) {
      if (!team.getName().equals("Team A")) {
        continue;
      }
      for (Room room : roomMap.values()) {
        for (Workday workday : workdayMap.values()) {
          resultList.add(RoomOwnership.builder()
              .room(room)
              .team(team)
              .workday(workday)
              .build());
        }
      }
    }

    return resultList;
  }

  private List<ReserveDeskPermission> initReserveDeskPermissions(Map<String, Workday> workdayMap,
      Map<String, Desk> deskMap, Map<String, Employee> employeeMap) {
    var resultList = new ArrayList<ReserveDeskPermission>();
    for (Workday workday : workdayMap.values()) {
      for (Desk desk : deskMap.values()) {
        for (Employee employee : employeeMap.values()) {
          if (employee.getTeam().getName().equals("Team A")) {
            resultList.add(ReserveDeskPermission.builder()
                .desk(desk)
                .employee(employee)
                .workday(workday)
                .build());
          }
        }
      }
    }
    return resultList;
  }

  private Map<String, Workday> initWorkdays() {
    LocalDate day1 = LocalDate.of(2022, 1, 1);
    return initWorkdays(day1, 100);
  }

  private Map<String, Workday> initWorkdays(LocalDate start, int totalDays) {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
    return IntStream.range(0, totalDays).mapToObj(i -> {
      return Workday.builder()
          .allocationStrategy("50-50")
          .date(start.plusDays(i))
          .build();
    }).collect(Collectors.toMap(wd -> dtf.format(wd.getDate()), wd -> wd));
  }

  private Map<String, Desk> initDesks() {
    Desk d1 = Desk.builder().name("Desk 1.1").build();
    Desk d2 = Desk.builder().name("Desk 1.2").build();
    Desk d3 = Desk.builder().name("Desk 1.3").build();
    Desk d4 = Desk.builder().name("Desk 1.4").build();
    Desk d5 = Desk.builder().name("Desk 2.1").build();
    Desk d6 = Desk.builder().name("Desk 3.1").build();
    Desk d7 = Desk.builder().name("Desk 3.2").build();

    return Map.of(d1.getName(), d1, d2.getName(), d2, d3.getName(), d3, d4.getName(), d4,
        d5.getName(), d5, d6.getName(), d6, d7.getName(), d7);
  }

  private Map<String, Room> initRooms() {
    Room r1 = Room.builder().name("Room 1").building("Building 42").build();
    Room r2 = Room.builder().name("Room 2").building("Building 42").build();
    Room r3 = Room.builder().name("Room 3").building("Building 42").build();

    return Map.of(r1.getName(), r1, r2.getName(), r2, r3.getName(), r3);
  }

  private Map<String, Workplace> initWorkplaces() {
    Workplace wp1 = Workplace.builder().name("Office").build();
    Workplace wp2 = Workplace.builder().name("From Home").build();
    Workplace wp3 = Workplace.builder().name("Field work").build();

    return Map.of(wp1.getName(), wp1, wp2.getName(), wp2, wp3.getName(), wp3);
  }

  private Map<String, Employee> initEmployees() {
    Employee employee1 = Employee.builder().firstName("Employee 1.1").lastName("lastname").build();
    Employee employee2 = Employee.builder().firstName("Employee 1.2").lastName("lastname").build();
    Employee employee3 = Employee.builder().firstName("Employee 1.3").lastName("lastname").build();
    Employee employee4 = Employee.builder().firstName("Employee 1.4").lastName("lastname").build();
    Employee employee5 = Employee.builder().firstName("Employee 1.5").lastName("lastname").build();
    Employee employee6 = Employee.builder().firstName("Employee 1.6").lastName("lastname").build();
    Employee employee7 = Employee.builder().firstName("Employee 2.1").lastName("lastname").build();
    Employee employee8 = Employee.builder().firstName("Employee 2.2").lastName("lastname").build();

    return Map.of(employee1.getFirstName(), employee1,
        employee2.getFirstName(), employee2,
        employee3.getFirstName(), employee3,
        employee4.getFirstName(), employee4,
        employee5.getFirstName(), employee5,
        employee6.getFirstName(), employee6,
        employee7.getFirstName(), employee7,
        employee8.getFirstName(), employee8
    );
  }

  private Map<String, Team> initTeams() {
    Team teamA = Team.builder().name("Team A").department("Cool department").build();
    Team teamB = Team.builder().name("Team B").department("Cool department").build();
    return Map.of(teamA.getName(), teamA, teamB.getName(), teamB);
  }
}
