package com.kmarinos.whereismydesk.domain.repository;

import com.kmarinos.whereismydesk.domain.model.DeskAvailability;
import com.kmarinos.whereismydesk.domain.model.DeskAvailability.DeskAvailabilityId;
import com.kmarinos.whereismydesk.domain.model.Workday;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeskAvailabilityRepository extends JpaRepository<DeskAvailability, DeskAvailabilityId> {

  List<DeskAvailability>findDeskAvailabilitiesByWorkdayAndAvailable(Workday workday,boolean available);
}
