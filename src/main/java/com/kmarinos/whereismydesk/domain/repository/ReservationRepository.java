package com.kmarinos.whereismydesk.domain.repository;

import com.kmarinos.whereismydesk.domain.model.Reservation;
import com.kmarinos.whereismydesk.domain.model.Reservation.ReservationId;
import com.kmarinos.whereismydesk.domain.model.Workday;
import com.kmarinos.whereismydesk.domain.model.Workplace;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationRepository extends JpaRepository<Reservation, ReservationId> {

  List<Reservation> findReservationsByWorkdayAndWorkplace(Workday workday, Workplace workplace);
}
