package com.kmarinos.whereismydesk.domain.repository;

import com.kmarinos.whereismydesk.domain.model.TeamWorkplaceOwnership;
import com.kmarinos.whereismydesk.domain.model.TeamWorkplaceOwnership.TeamWorkplaceOwnershipId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamWorkplaceOwnershipRepository extends JpaRepository<TeamWorkplaceOwnership, TeamWorkplaceOwnershipId> {

}
