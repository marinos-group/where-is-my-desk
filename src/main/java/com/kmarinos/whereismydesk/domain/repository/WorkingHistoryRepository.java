package com.kmarinos.whereismydesk.domain.repository;

import com.kmarinos.whereismydesk.domain.model.WorkingHistory;
import com.kmarinos.whereismydesk.domain.model.WorkingHistory.WorkingHistoryId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkingHistoryRepository extends JpaRepository<WorkingHistory, WorkingHistoryId> {

}
