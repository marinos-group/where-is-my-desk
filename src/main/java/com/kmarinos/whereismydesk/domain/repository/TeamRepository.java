package com.kmarinos.whereismydesk.domain.repository;

import com.kmarinos.whereismydesk.domain.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team,Long> {

}
