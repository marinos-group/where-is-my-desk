package com.kmarinos.whereismydesk.domain.repository;

import com.kmarinos.whereismydesk.domain.model.ReserveDeskPermission;
import com.kmarinos.whereismydesk.domain.model.ReserveDeskPermission.ReserveDeskPermissionId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReserveDeskPermissionRepository extends
    JpaRepository<ReserveDeskPermission, ReserveDeskPermissionId> {

}
