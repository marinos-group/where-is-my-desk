package com.kmarinos.whereismydesk.domain.repository;

import com.kmarinos.whereismydesk.domain.model.Desk;
import com.kmarinos.whereismydesk.domain.model.Employee;
import com.kmarinos.whereismydesk.domain.model.Room;
import com.kmarinos.whereismydesk.domain.model.Workday;
import com.kmarinos.whereismydesk.domain.model.Workplace;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DeskRepository extends JpaRepository<Desk,Long> {
  @Query("select rdp.desk from ReserveDeskPermission rdp where rdp.employee=:employee and rdp.workday=:workday")
  List<Desk> findDesksForEmployeeForDay(@Param("employee") Employee employee,
      @Param("workday") Workday workday);
  List<Desk> findDesksByRoomIn(List<Room> rooms);
  @Query("select d from Desk d where d.room.workplace=:workplace")
  List<Desk> findDesksByWorkplace(Workplace workplace);

}
