package com.kmarinos.whereismydesk.domain.repository;

import com.kmarinos.whereismydesk.domain.model.CollisionResolution;
import com.kmarinos.whereismydesk.domain.model.CollisionResolution.CollisionResolutionId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CollisionResolutionRepository extends JpaRepository<CollisionResolution, CollisionResolutionId> {

}
