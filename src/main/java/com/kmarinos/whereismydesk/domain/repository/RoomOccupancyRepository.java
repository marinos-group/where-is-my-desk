package com.kmarinos.whereismydesk.domain.repository;

import com.kmarinos.whereismydesk.domain.model.Room;
import com.kmarinos.whereismydesk.domain.model.RoomOccupancy;
import com.kmarinos.whereismydesk.domain.model.RoomOccupancy.RoomOccupancyId;
import com.kmarinos.whereismydesk.domain.model.Workday;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomOccupancyRepository extends JpaRepository<RoomOccupancy, RoomOccupancyId> {

  List<RoomOccupancy> findRoomOccupanciesByWorkdayAndRoomIn(Workday workday, List<Room> room);
}
