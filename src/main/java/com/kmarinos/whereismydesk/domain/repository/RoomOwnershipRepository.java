package com.kmarinos.whereismydesk.domain.repository;

import com.kmarinos.whereismydesk.domain.model.RoomOwnership;
import com.kmarinos.whereismydesk.domain.model.RoomOwnership.RoomOwnershipId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomOwnershipRepository extends JpaRepository<RoomOwnership, RoomOwnershipId> {

}
