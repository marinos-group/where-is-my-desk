package com.kmarinos.whereismydesk.domain.repository;

import com.kmarinos.whereismydesk.domain.model.Desk;
import com.kmarinos.whereismydesk.domain.model.Workday;
import com.kmarinos.whereismydesk.domain.model.Workplace;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface WorkplaceRepository extends JpaRepository<Workplace,Long> {

  Optional<Workplace> findWorkplaceByName(String name);


}
