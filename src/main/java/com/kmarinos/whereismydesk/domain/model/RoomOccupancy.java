package com.kmarinos.whereismydesk.domain.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class RoomOccupancy {

  @EmbeddedId
  RoomOccupancyId id;

  @ManyToOne
  @MapsId("roomId")
  @Include
  Room room;
  @ManyToOne
  @MapsId("workdayId")
  @Include
  Workday workday;
  int desksAvailable;

  @Builder
  public RoomOccupancy(Room room, Workday workday, int desksAvailable) {
    this.id = new RoomOccupancyId(room.getId(), workday.getId());
    this.room = room;
    this.workday = workday;
    this.desksAvailable = desksAvailable;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Embeddable
  public static class RoomOccupancyId implements Serializable {

    Long roomId;
    Long workdayId;
  }
}
