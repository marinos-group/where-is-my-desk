package com.kmarinos.whereismydesk.domain.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class RoomOwnership implements Serializable {

  @EmbeddedId
  RoomOwnershipId id;

  @ManyToOne
  @MapsId("teamId")
  @Include
  Team team;
  @ManyToOne
  @MapsId("roomId")
  @Include
  Room room;
  @ManyToOne
  @MapsId("workdayId")
  @Include
  Workday workday;

  @Builder
  public RoomOwnership(Team team, Room room, Workday workday) {
    this.id = new RoomOwnershipId(team.getId(), room.getId(), workday.getId());
    this.team = team;
    this.room = room;
    this.workday = workday;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Embeddable
  public static class RoomOwnershipId implements Serializable {

    Long teamId;
    Long roomId;
    Long workdayId;
  }
}
