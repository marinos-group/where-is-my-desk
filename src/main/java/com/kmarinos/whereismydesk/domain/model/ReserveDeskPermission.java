package com.kmarinos.whereismydesk.domain.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ReserveDeskPermission implements Serializable {

  @EmbeddedId
  ReserveDeskPermissionId id;

  @ManyToOne
  @MapsId("employeeId")
  @Include
  Employee employee;
  @ManyToOne
  @MapsId("deskId")
  @Include
  Desk desk;
  @ManyToOne
  @MapsId("workdayId")
  @Include
  Workday workday;
  String assignedBy;

  @Builder
  public ReserveDeskPermission(Employee employee, Desk desk,
      Workday workday, String assignedBy) {
    this.id = new ReserveDeskPermissionId(employee.getId(), desk.getId(), workday.getId());
    this.employee = employee;
    this.desk = desk;
    this.workday = workday;
    this.assignedBy = assignedBy == null ? "room" : assignedBy;
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  @Embeddable
  public static class ReserveDeskPermissionId implements Serializable {

    Long employeeId;
    Long deskId;
    Long workdayId;
  }
}
