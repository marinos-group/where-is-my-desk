package com.kmarinos.whereismydesk.domain.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Reservation implements Serializable {

  @EmbeddedId
  ReservationId id;

  @MapsId("employeeId")
  @ManyToOne
      @Include
  Employee employee;
  @MapsId("workdayId")
  @ManyToOne
      @Include
  Workday workday;
  @ManyToOne
  Workplace workplace;
  @ManyToOne
  Desk desk;
  @CreationTimestamp
  Date createdAt;
  @UpdateTimestamp
  Date updatedAt;

  @Builder
  public Reservation(Employee employee, Workday workday,
      Workplace workplace, Desk desk) {
    this.id = new ReservationId(employee.getId(), workday.getId());
    this.employee = employee;
    this.workday = workday;
    this.workplace = workplace;
    this.desk = desk;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Embeddable
  public static class ReservationId implements Serializable {

    Long employeeId;
    Long workdayId;
  }
}
