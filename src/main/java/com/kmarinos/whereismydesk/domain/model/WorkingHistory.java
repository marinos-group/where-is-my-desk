package com.kmarinos.whereismydesk.domain.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class WorkingHistory implements Serializable {

  @EmbeddedId
  WorkingHistoryId id;

  @Include
  @MapsId("employeeId")
  @ManyToOne
  Employee employee;
  @Include
  @MapsId("workdayId")
  @ManyToOne
  Workday workday;
  @ManyToOne
  Workplace workplace;

  @Builder
  public WorkingHistory(Employee employee, Workday workday,
      Workplace workplace) {
    this.id = new WorkingHistoryId(employee.getId(), workday.getId());
    this.employee = employee;
    this.workday = workday;
    this.workplace = workplace;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Embeddable
  public static class WorkingHistoryId implements Serializable {

    Long employeeId;
    Long workdayId;
  }
}
