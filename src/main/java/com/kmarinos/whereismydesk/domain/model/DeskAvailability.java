package com.kmarinos.whereismydesk.domain.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class DeskAvailability {
  @EmbeddedId DeskAvailabilityId id;

  @ManyToOne
  @MapsId("deskId")
  @Include
  Desk desk;
  @ManyToOne
  @MapsId("workdayId")
  @Include
  Workday workday;
  boolean available;
  String availabilityHint;

  @Builder
  public DeskAvailability(Desk desk, Workday workday, boolean available,
      String availabilityHint) {
    this.id=new DeskAvailabilityId(desk.getId(), workday.getId());
    this.desk = desk;
    this.workday = workday;
    this.available = available;
    this.availabilityHint = availabilityHint;
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  @Embeddable
  public static class DeskAvailabilityId implements Serializable{
    Long deskId;
    Long workdayId;
  }
}
