package com.kmarinos.whereismydesk.domain.model;

import com.kmarinos.whereismydesk.domain.model.Reservation.ReservationId;
import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class CollisionResolution implements Serializable {

  @EmbeddedId
  @Include
  CollisionResolutionId id;

  @ManyToOne
  @MapsId("reservationId")
  @Include
  Reservation reservation;
  boolean wasOverruled;
  boolean resolved;
  String tieBreakerStrategy;
  String tieBreakerInfo;

  @Builder
  public CollisionResolution(String collisionId,
      Reservation reservation, boolean wasOverruled, boolean resolved,
      String tieBreakerStrategy, String tieBreakerInfo) {
    this.id = new CollisionResolutionId(collisionId, reservation.getId());
    this.reservation = reservation;
    this.wasOverruled = wasOverruled;
    this.resolved = resolved;
    this.tieBreakerStrategy = tieBreakerStrategy;
    this.tieBreakerInfo = tieBreakerInfo;
  }


  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Embeddable
  public static class CollisionResolutionId implements Serializable {

    String collisionId;
    ReservationId reservationId;
  }
}
