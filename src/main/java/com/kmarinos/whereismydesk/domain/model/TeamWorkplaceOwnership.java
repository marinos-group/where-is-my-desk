package com.kmarinos.whereismydesk.domain.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TeamWorkplaceOwnership implements Serializable {

  @EmbeddedId
  TeamWorkplaceOwnershipId id;

  @ManyToOne
  @MapsId("teamId")
  @Include
  Team team;
  @MapsId("workplaceId")
  @ManyToOne
  @Include
  Workplace workplace;
  @ManyToOne
  @MapsId("workdayId")
  @Include
  Workday workday;

  @Builder
  public TeamWorkplaceOwnership(Team team, Workplace workplace,
      Workday workday) {
    this.id = new TeamWorkplaceOwnershipId(team.getId(), workplace.getId(), workday.getId());
    this.team = team;
    this.workplace = workplace;
    this.workday = workday;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Embeddable
  public static class TeamWorkplaceOwnershipId implements Serializable {

    Long teamId;
    Long workplaceId;
    Long workdayId;
  }
}
