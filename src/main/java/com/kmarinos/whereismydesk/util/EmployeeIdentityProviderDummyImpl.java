package com.kmarinos.whereismydesk.util;

import com.kmarinos.whereismydesk.domain.model.Employee;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class EmployeeIdentityProviderDummyImpl implements EmployeeIdentityProvider{

  @Override
  public Employee getQualifiedNameFromContext(Map<String, Object> context) {
    return Employee.builder()
        .firstName("Employee 1.1")
        .lastName("lastname")
        .build();
  }
}
