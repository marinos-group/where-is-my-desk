package com.kmarinos.whereismydesk.util;

import com.kmarinos.whereismydesk.domain.model.Employee;
import java.util.Map;

public interface EmployeeIdentityProvider {

  public Employee getQualifiedNameFromContext(Map<String, Object> context);
}
