package com.kmarinos.whereismydesk.exceptionHandling.exceptions;

public class OperationNotAllowedException extends RuntimeException{
    public OperationNotAllowedException(String message){
        super(message);
    }
}
