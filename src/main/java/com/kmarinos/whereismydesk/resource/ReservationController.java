package com.kmarinos.whereismydesk.resource;

import com.kmarinos.whereismydesk.domain.model.Desk;
import com.kmarinos.whereismydesk.domain.model.DeskAvailability;
import com.kmarinos.whereismydesk.domain.model.Employee;
import com.kmarinos.whereismydesk.domain.model.Reservation;
import com.kmarinos.whereismydesk.domain.model.Room;
import com.kmarinos.whereismydesk.domain.model.Workday;
import com.kmarinos.whereismydesk.domain.model.Workplace;
import com.kmarinos.whereismydesk.resource.dto.DayDeskAvailability;
import com.kmarinos.whereismydesk.resource.dto.DeskInformation;
import com.kmarinos.whereismydesk.service.DeskService;
import com.kmarinos.whereismydesk.service.EmployeeService;
import com.kmarinos.whereismydesk.service.ReservationService;
import com.kmarinos.whereismydesk.service.RoomService;
import com.kmarinos.whereismydesk.service.WorkdayService;
import com.kmarinos.whereismydesk.service.WorkplaceService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/reservations")
@RequiredArgsConstructor
public class ReservationController {

  private final DeskService deskService;
  private final ReservationService reservationService;
  private final RoomService roomService;
  private final EmployeeService employeeService;
  private final WorkdayService workdayService;
  private final WorkplaceService workplaceService;

  @GetMapping("{day}/{workplace}")
  public DayDeskAvailability getDeskAvailability(@PathVariable("day") String day,
      @PathVariable("workplace") String workplace, HttpServletRequest request) {
    String ipAddress = request.getHeader("X-FORWARDED-FOR");
    if (ipAddress == null) {
      ipAddress = request.getRemoteAddr();
    }

    return reconcileDesks(workdayService.fetchWorkdayWithDateString(day),
    workplaceService.fetchWorkplaceWithName(workplace),
    employeeService.fetchEmployeeFromRequest(Map.of("ipAddress", ipAddress)));

  }

  private DayDeskAvailability reconcileDesks(Workday day, Workplace workplace, Employee employee) {
    //find all available desks for me for the day
    List<Desk> whiteDeskList = deskService.fetchDesksForEmployeeForDay(employee, day);
    //ignore desks that don't belong to workplace
    List<Desk> desksInWorkplace = deskService.fetchAllDesksOfWorkplace(workplace);
    whiteDeskList.removeIf(d -> !desksInWorkplace.contains(d));

    //find which desks are already reserved
    List<Desk> blackDeskList = reservationService.fetchReservationsForWorkplaceAndDay(workplace,
            day)
        .stream().map(Reservation::getDesk).filter(Objects::nonNull).distinct()
        .collect(Collectors.toList());

    //exclude desks that are out of order
    List<DeskAvailability> deskAvailabilityList = deskService.findUnavailableDesks(desksInWorkplace,
        day);
    List<Desk> unavailableDesks = deskAvailabilityList.stream().map(DeskAvailability::getDesk)
        .distinct().collect(
            Collectors.toList());

    //exclude desks where maximum room occupancy has been reached
    List<Desk> allDesksOfRelevantRooms = deskService.fetchAllDesksOfRooms(
        blackDeskList.stream().map(Desk::getRoom).distinct().collect(
            Collectors.toList()));
    Map<Room, Long> roomOccupancyMap = allDesksOfRelevantRooms.stream()
        .collect(Collectors.groupingBy(Desk::getRoom, Collectors.counting()));
    Map<Room, Integer> roomMaxOccupancyMap = roomService.fetchRoomOccupancies(
        new ArrayList<>(roomOccupancyMap.keySet()), day);

    List<Room> unavailableRooms = roomOccupancyMap.entrySet().stream().filter(entry -> {
      return entry.getValue() >= roomMaxOccupancyMap.get(entry.getKey());
    }).map(Entry::getKey).collect(Collectors.toList());

    //Reconcile desks
    List<DeskInformation> deskInformationList = new ArrayList<>();
    whiteDeskList.stream().filter(d -> !blackDeskList.contains(d))
        .filter(d -> !unavailableRooms.contains(d.getRoom()))
        .filter(d -> !unavailableDesks.contains(d))
        .forEach(
            d -> {
              deskInformationList.add(new DeskInformation(d, true, ""));
            });
    deskAvailabilityList.stream().distinct().forEach(da -> {
      deskInformationList.add(new DeskInformation(da.getDesk(), false, da.getAvailabilityHint()));
    });
    blackDeskList.stream().distinct()
        .filter(d -> !unavailableDesks.contains(d))
        .forEach(d -> {
          deskInformationList.add(new DeskInformation(d, false, "user_reserved"));
        });
    allDesksOfRelevantRooms.stream().filter(d -> unavailableRooms.contains(d.getRoom()))
        .filter(d -> !blackDeskList.contains(d))
        .filter(d -> !unavailableDesks.contains(d))
        .distinct().forEach(d -> {
          deskInformationList.add(new DeskInformation(d, false, "max_occupancy"));
        });
    return new DayDeskAvailability(day, workplace, deskInformationList);
  }

}
