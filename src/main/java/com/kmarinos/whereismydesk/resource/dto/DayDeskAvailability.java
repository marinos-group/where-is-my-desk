package com.kmarinos.whereismydesk.resource.dto;

import com.kmarinos.whereismydesk.domain.model.Workday;
import com.kmarinos.whereismydesk.domain.model.Workplace;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DayDeskAvailability {

  Workday workday;
  Workplace workplace;
  List<DeskInformation> desks;
}
