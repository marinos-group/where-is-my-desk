package com.kmarinos.whereismydesk.resource.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.kmarinos.whereismydesk.domain.model.Desk;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DeskInformation {

  Desk desk;
  boolean available;
  @JsonInclude(Include.NON_EMPTY)
  String reason;
}
