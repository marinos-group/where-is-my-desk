package com.kmarinos.whereismydesk.service;

import com.kmarinos.whereismydesk.domain.model.Workday;
import com.kmarinos.whereismydesk.domain.repository.WorkDayRepository;
import com.kmarinos.whereismydesk.exceptionHandling.exceptions.CannotParseInputException;
import com.kmarinos.whereismydesk.exceptionHandling.exceptions.EntityNotFoundException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WorkdayService {

  private final WorkDayRepository workDayRepository;
  private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");

  public Workday fetchWorkdayWithDateString(String dateString) {
    try {
      final var date = LocalDate.parse(dateString, dateTimeFormatter);
      return workDayRepository.findWorkdayByDate(date)
          .orElseThrow(() -> new EntityNotFoundException(Workday.class, "dateString (yyyyMMdd)"));
    } catch (DateTimeException e) {
      throw new CannotParseInputException(LocalDate.class, dateString,
          "yyyyMMdd");
    }
  }

}
