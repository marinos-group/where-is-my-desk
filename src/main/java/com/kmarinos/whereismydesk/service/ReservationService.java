package com.kmarinos.whereismydesk.service;

import com.kmarinos.whereismydesk.domain.model.Reservation;
import com.kmarinos.whereismydesk.domain.model.Workday;
import com.kmarinos.whereismydesk.domain.model.Workplace;
import com.kmarinos.whereismydesk.domain.repository.ReservationRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReservationService {

  private final ReservationRepository reservationRepository;

  //show desks as booked
  public List<Reservation> fetchReservationsForWorkplaceAndDay(Workplace workplace, Workday workday){
    return reservationRepository.findReservationsByWorkdayAndWorkplace(workday,workplace);
  }
}
