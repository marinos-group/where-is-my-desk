package com.kmarinos.whereismydesk.service;

import com.kmarinos.whereismydesk.domain.model.Desk;
import com.kmarinos.whereismydesk.domain.model.DeskAvailability;
import com.kmarinos.whereismydesk.domain.model.Employee;
import com.kmarinos.whereismydesk.domain.model.Room;
import com.kmarinos.whereismydesk.domain.model.Workday;
import com.kmarinos.whereismydesk.domain.model.Workplace;
import com.kmarinos.whereismydesk.domain.repository.DeskAvailabilityRepository;
import com.kmarinos.whereismydesk.domain.repository.DeskRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeskService {

  private final DeskRepository deskRepository;
  private final DeskAvailabilityRepository deskAvailabilityRepository;

  public List<Desk> fetchDesksForEmployeeForDay(Employee employee, Workday workday) {
    return deskRepository.findDesksForEmployeeForDay(employee, workday);
  }

  public List<Desk> fetchAllDesksOfRooms(List<Room> rooms) {
    return deskRepository.findDesksByRoomIn(rooms);
  }

  public List<Desk> fetchAllDesksOfWorkplace(Workplace workplace) {
    return deskRepository.findDesksByWorkplace(workplace);
  }

  public List<DeskAvailability> findUnavailableDesks(List<Desk> desks, Workday workday) {
    return deskAvailabilityRepository.findDeskAvailabilitiesByWorkdayAndAvailable(workday, false).stream()
        .filter(deskAvailability -> {
          return desks.contains(deskAvailability.getDesk());
        }).collect(Collectors.toList());
  }
}
