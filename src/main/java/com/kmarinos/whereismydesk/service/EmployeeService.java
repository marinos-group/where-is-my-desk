package com.kmarinos.whereismydesk.service;

import com.kmarinos.whereismydesk.domain.model.Employee;
import com.kmarinos.whereismydesk.domain.repository.EmployeeRepository;
import com.kmarinos.whereismydesk.exceptionHandling.exceptions.EntityNotFoundException;
import com.kmarinos.whereismydesk.util.EmployeeIdentityProvider;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EmployeeService {

  private final EmployeeRepository employeeRepository;
  private final EmployeeIdentityProvider employeeIdentityProvider;

  public Employee fetchEmployeeFromRequest(Map<String,Object> context){
    Employee example=employeeIdentityProvider.getQualifiedNameFromContext(context);
    return this.fetchEmployee(example.getFirstName(), example.getLastName());

  }

  public Employee fetchEmployee(String firstname, String lastname) {

    return employeeRepository.findEmployeeByFirstNameAndLastName(firstname, lastname)
        .orElseThrow(
            () -> new EntityNotFoundException(Employee.class, "first name", firstname, "last name",
                lastname));
  }

}
