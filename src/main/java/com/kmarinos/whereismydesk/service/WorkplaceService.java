package com.kmarinos.whereismydesk.service;

import com.kmarinos.whereismydesk.domain.model.Desk;
import com.kmarinos.whereismydesk.domain.model.Workday;
import com.kmarinos.whereismydesk.domain.model.Workplace;
import com.kmarinos.whereismydesk.domain.repository.WorkplaceRepository;
import com.kmarinos.whereismydesk.exceptionHandling.exceptions.EntityNotFoundException;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WorkplaceService {

  private final WorkplaceRepository workplaceRepository;

  public Workplace fetchWorkplaceWithName(String name) {
    return workplaceRepository.findWorkplaceByName(name).orElseThrow(
        () -> new EntityNotFoundException(Workplace.class, "name",name));
  }

}
