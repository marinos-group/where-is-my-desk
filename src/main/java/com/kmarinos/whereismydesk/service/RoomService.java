package com.kmarinos.whereismydesk.service;

import com.kmarinos.whereismydesk.domain.model.Room;
import com.kmarinos.whereismydesk.domain.model.RoomOccupancy;
import com.kmarinos.whereismydesk.domain.model.Workday;
import com.kmarinos.whereismydesk.domain.repository.RoomOccupancyRepository;
import com.kmarinos.whereismydesk.domain.repository.RoomRepository;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoomService {

  private final RoomRepository roomRepository;
  private final RoomOccupancyRepository roomOccupancyRepository;

  public Map<Room,Integer> fetchRoomOccupancies(List<Room> rooms, Workday workday){
    return roomOccupancyRepository.findRoomOccupanciesByWorkdayAndRoomIn(workday,rooms).stream().collect(
        Collectors.toMap(RoomOccupancy::getRoom,RoomOccupancy::getDesksAvailable));
  }
}
