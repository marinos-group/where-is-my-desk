import { useMemo, useState } from "react";
import "./Calendar.css";
import { default as ReactCalendar } from "react-calendar";
import {
  differenceInCalendarDays,
  getDaysInMonth,
  isAfter,
  isBefore,
} from "date-fns";

function isSameDay(a, b) {
  return differenceInCalendarDays(a, b) === 0;
}
const Calendar = () => {
  const [value, onChange] = useState(new Date());
  const today = useMemo(() => {
    return new Date();
  }, []);
  return (
    <div className="flex">
      <div className="mx-auto my-4">
        <ReactCalendar
          onChange={onChange}
          value={value}
          showWeekNumbers={true}
          onClickDay={(value, event) => console.log("Clicked day: ", value)}
          tileClassName={({ activeStartDate, date, view }) => {
            if (view === "month") {
              if (isBefore(date, today) && !isSameDay(date, today)) {
                return "date-no-conflict";
              } else {
                if (date.getDate() % 5 === 0) {
                  return "date-has-conflict";
                } else {
                  return null;
                }
              }
            } else {
              return null;
            }
          }}
          tileContent={({ activeStartDate, date, view }) =>
            view === "month" ? (
              (date.getDay() + 1) % 2 === 0 ? (
                <div
                  className="h-8 bg-blue-200 mt-2 mx-1"
                  onClick={() => console.log("Clicked on inner tile")}
                ></div>
              ) : (
                <div className="flex mt-2 mx-1 flex-row">
                  <div className="h-8 flex-1 bg-blue-200 m-auto"></div>
                  <div className="h-8 flex-1 bg-blue-400 m-auto"></div>
                </div>
              )
            ) : null
          }
        />
      </div>
    </div>
  );
};
export default Calendar;
