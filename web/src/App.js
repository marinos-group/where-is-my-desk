import Calendar from "./components/Calendar";
import useStore from "./store/useStore";
import shallow from "zustand/shallow";
import { useCallback, useEffect } from "react";

function App() {
  const [bears, increasePopulation] = useStore(
    (state) => [state.bears, state.increasePopulation],
    shallow
  );
  const [deskAvailability, fetchDeskAvailability] = useStore(
    (state) => [state.deskAvailability, state.fetchDeskAvailability],
    shallow
  );
  const init = useCallback(() => {
    fetchDeskAvailability("20220122", "Office");
    console.log(deskAvailability);
  }, [fetchDeskAvailability]);
  useEffect(() => {
    init();
  }, [init]);
  const renderDesks = () => {
    if (!deskAvailability || !deskAvailability.desks) {
      return null;
    }
    return deskAvailability.desks.map((desk) => {
      return (
        <div key={desk.desk.name}>
          {console.log(desk.available)} {desk.desk.name}:
          {desk.available ? "Available" : `Not available (${desk.reason})`}
        </div>
      );
    });
  };
  return (
    <div className="container mx-auto">
      <Calendar />
      <div>{renderDesks()}</div>
    </div>
  );
}

export default App;
