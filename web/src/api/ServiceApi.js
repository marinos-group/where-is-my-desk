import axios from "axios";

const baseURL = process.env.REACT_APP_SERVICE_API_URL_IPV4
  ? process.env.REACT_APP_SERVICE_API_URL_IPV4
  : "http://localhost";
const port = process.env.REACT_APP_SERVICE_API_PORT
  ? process.env.REACT_APP_SERVICE_API_PORT
  : "8080";
const rootPath = process.env.REACT_APP_SERVICE_API_ROOT_PATH
  ? process.env.REACT_APP_SERVICE_API_ROOT_PATH
  : "api";

export default axios.create({
  baseURL: `${baseURL}:${port}/${rootPath}`,
});
