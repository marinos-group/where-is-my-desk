import create from "zustand";
import ServiceApi from "../api/ServiceApi";

const useStore = create((set) => ({
  bears: 0,
  increasePopulation: () => set((state) => ({ bears: state.bears + 1 })),
  removeAllBears: () => set({ bears: 0 }),
  deskAvailability: {},
  fetchDeskAvailability: async (day, workplace) =>
    ServiceApi.get(`reservations/${day}/${workplace}`).then((response) =>
      set({ deskAvailability: response.data })
    ),
}));

export default useStore;
